#!/bin/bash
#archive_logs.bash
# This script will take a log file that has been rotated and push it off to a seperate directory compressed, and add the same date
# of last modification
# Constructor

##### EDITABLE CONFIGURATION #####
# absolute path to log file ie: /foo/bar.log
path_to_log=
# command to run to compress file, 
compress_string="/usr/bin/bzip2 -9"
compress_extention=bz2
# target holding directory
tar_dir=
##################################

function set_up()
{
	if [ ! -z $tar_dir ]
		then
		mkdir -p $tar_dir
	fi
}

function main()
{
	for log_file in $( ls $path_to_log.* | grep -v *.bz2)
	do
		t_stamp=$(stat $log_file | grep Modify | awk '{print $2, $3}' | sed 's/ /-/' | sed 's/\..*$//')
		log_name=$(basename $log_file | awk -F . '{print $1,$2}' | sed 's/ /./g').$t_stamp.$compress_extention
		cat $log_file | $compress_string > $tar_dir/$log_name
		rm -f $log_file
	done
}

set_up
main
