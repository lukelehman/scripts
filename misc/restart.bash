#!/bin/bash
#restart.bash
#
# This script uses mco (mcollective) to send service commands to machines based upon the entries located in a csv.
# CSV template:
# <fqdn>,<service>,<stop|start|restart>
# <fqdn>,<service>,<stop|start|restart>

##CONFIG##
csv_loc=/usr/local/etc/restart.csv


function send_cmd()
{
        mco service $2 $3 -I $1
}

for line_num in $(cat $csv_loc)
do
        send_cmd $(echo $line_num | awk -F, '{print $1, $2, $3}')
done
