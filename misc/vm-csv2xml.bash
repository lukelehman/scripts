#!/bin/bash
#vm-csv2xml.bash

source_file=
output_file=
#global vmware options
opt_nic_network=test_nn
opt_datacenter=test_dc

function scrape-gen
{
    # This function cycle's through each line in the csv and assigns each field to an option value
    # which is used to generate the xml for each host
	for source_line in $(cat $source-file)
	do
        opt_vmname=$(echo $source_line | awk -F',' '{print $x')
		opt_datastore=$(echo $source_line | awk -F',' '{print $x')
		opt_vmhost=$(echo $source_line | awk -F',' '{print $x')
		opt_disksize=$(echo $source_line | awk -F',' '{print $x')  #in kb
		opt_memory=$(echo $source_line | awk -F',' '{print $x')    #in MB
		opt_num_cpus=$(echo $source_line | awk -F',' '{print $x')
        opt_guestid=$(echo $source_line | awk -F',' '{print $x')

        echo"         <Virtual-Machine>" >> $output_file
        echo"            <Name>$opt_vmname</Name>" >> $output_file
        echo"                <Host>$opt_vmhost</Host>" >> $output_file
        echo"            <Datacenter>$opt_datacenter</Datacenter>" >> $output_file
        echo"                <Guest-Id>$opt_guestid</Guest-Id>" >> $output_file
        echo"                <Datastore>$opt_datastore</Datastore>" >> $output_file
        echo"                <Disksize>$opt_disksize</Disksize>" >> $output_file
        echo"                <Memory>$opt_memory</Memory>" >> $output_file
        echo"                <Number-of-Processor>$opt_num_cpus</Number-of-Processor>" >> $output_file
        echo"                <Nic-Network>$opt_nic_network</Nic-Network>" >> $output_file
        echo"                <Nic-Poweron>0</Nic-Poweron>" >> $output_file
        echo"         </Virtual-Machine>" >> $output_file
}

# Generate files heading
echo "<?xml version=\"1.0\"?>" > $output_file
echo "  <Virtual-Machines>" >> $output_file
# Call the scrape-gen function to generate each vm properties
scrape-gen
echo "  </Virtual-Machines>" >> $output_file
