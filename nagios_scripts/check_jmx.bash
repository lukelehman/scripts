#!/bin/bash
#check_jmx.bash
# Nagios plugin to monitor Java JMX. Will retrive a single bean count and check in three different types of modes
#
# This script requires jmxterm.jar as well as having $jmx_loc pointed to the jar
#
# Modes of operation: There are three modes of operation which will return different results based upon user selected input
# The default mode is is raw value comparison.
# The other modes available are for time comparison:
#   -d Difference between the first and last instance in the time frame specified
#   -A Average between all data gathered in the specified time frame
#
# usage:
# -A Average mode of operation. Will take the average of all data points
# -d Difference mode of operation. Will take the difference between the first and last instance in the time frame specified
# -t Time period to use for the sample data
# -b bean
# -a attribute
# -w Warn threshold
# -c crit threshold
# -H Hostname
# -p port jmx is listening on
# -h Help, will print this menu
#
# Example:
# ./check_jmx_nss.bash -b com.yt.nss.smpp.server:type=Pdu -a TotalPduRequest -H localhost -p 6114 -w 5 -c 6
# $USER1$/check_jmx_nss.bash -b $ARG1$ -a $ARG2$ -H $HOSTADDRESS$ -p $ARG3$ -w $ARG4$ -c $ARG5$
# $USER1$/check_jmx_nss.bash -b $ARG1$ -a $ARG2$ -H $HOSTADDRESS$ -p $ARG3$ -w $ARG4$ -c $ARG5$ -t $ARG6$ -A
# $USER1$/check_jmx_nss.bash -b $ARG1$ -a $ARG2$ -H $HOSTADDRESS$ -p $ARG3$ -w $ARG4$ -c $ARG5$ -t $ARG6$ -d
#
#



# hard config, change if needed
jmx_loc=/usr/lib64/nagios/plugins/misc/jmxterm.jar
java_loc=$(which java)
tmp_path=/tmp/jmxmon
# nagios exit states
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# check if directory exists, if not create it


function find_mode()
{
  # This function senses the mode in which to run and will then call the necesary collection of
  # functions to operate the script in that mode
  if [ "$mode_avg" = 1 ] && [ "$mode_diff" = 1 ]
    then
    echo "Select only one mode of operation please"
    exit 100
  else
    if [ "$mode_avg" = 1 ]
      then
      #do the mode avg stuff
      tmp_path=$tmp_path.avg
      get_jmx_value
      average
      value=$(echo $final_avg)
      clean
      exit_check
    elif [ "$mode_diff" = 1 ]
      then
      # do the diff stuff
      tmp_path=$tmp_path.diff
      get_jmx_value
      difference
      value=$(echo $difference_count)
      clean
      exit_check
    else
      # do the default stuff
      get_jmx_value
      rm -rf --preserve-root $tmp_path
      exit_check
    fi
  fi
}

function usage()
{
  echo "usage:"
  echo "  -A Average mode of operation. Will take the average of all data points"
  echo "  -d Difference mode of operation. Will take the difference between the first and last instance in the time frame specified"
  echo "  -t Time period to use for the sample data"
  echo "  -b bean"
  echo "  -a attribute"
  echo "  -w Warn threshold"
  echo "  -c crit threshold"
  echo "  -H Hostname"
  echo "  -p port the jmx is listening on"
  echo "  -h Help, will print this menu"
}

function get_jmx_value()
{
  # This function will create a folder in $tmp_path, retrive the jmx value and store it in a file with the current timestamp
  if [ ! -d $tmp_path ]
    then
    mkdir $tmp_path
  fi
  value=$(echo "get -b $bean $attribute" | $java_loc -jar $jmx_loc -l $hn:$port -v silent -n | /usr/bin/head -1 | /bin/awk '{print $3}' | /bin/sed 's/;//')
  value=$(echo $value | sed 's/([0-9]+\.[0-9]{2})[0-9]+//g')
  if [ -z "$value" ]
  then
    value=0
  fi
  timestamp=$(date +%s)
  echo $value > $tmp_path/$timestamp
}

function difference()
{
  # Difference between first occurance and last occurance
  first_count=$(cat $tmp_path/$(ls -rt $tmp_path | tail -1))
  last_count=$(cat $tmp_path/$(ls -rt $tmp_path | head -1))
  difference_count=$(echo "$first_count-$last_count" | bc)
}

function average()
{
  # Average of all over the specified time period

  running_total=0
  file_count=0
  for file in $(ls $tmp_path)
  do
    running_total=$(($running_total + $(cat $tmp_path/$file) ))
    let "file_count++"
  done
  final_avg=$(echo $running_total / $file_count | bc)
}

function exit_check()
{
  if [ "$value" -ge "$warn" ] && [ "$value" -ge 0 ]
    then
      if [ "$value" -ge "$crit" ]
        then
        echo "CRITICAL, value has exceeded the threshold | count=$value"
        exit $STATE_CRITICAL
      fi
      echo "WARNING, value has exceeded the threshold | count=$value"
      exit $STATE_WARNING
    else
    if [ "$value" -lt 0 ]
      then
      echo "This script has no idea what to do with this value and is therefore unkown | count=$value"
      exit $STATE_OK
#      exit $STATE_UNKNOWN
    fi
    echo "OK | count=$value"
    exit $STATE_OK
  fi
}

function clean()
{
  # Cleanup
  # find files in $tmp_path over user defined period of time in minutes and delete
  find $tmp_path -cmin $time_period -exec rm -f --preserve-root {} \;
}

# Get command line arguments and assign them to variables
while getopts ":Adt:b:a:w:c:H:p:" opt
do
  case "$opt" in
    A) mode_avg=1;;
    d) mode_diff=1;;
    t) time_period=${OPTARG};;
    b) bean=${OPTARG};;
    a) attribute=${OPTARG};;
    w) warn=${OPTARG};;
    c) crit=${OPTARG};;
    H) hn=${OPTARG};;
    p) port=${OPTARG};;
    h) usage;;
    *) echo "unrecognized option"
       usage
       exit 99;;
  esac
done

# Call the mode function which will check for the mode selected and then process the jmx output in one of the specified modes.

find_mode
