#!/bin/bash
#nagcol.sh

loghome=/var/local/nagcol   #base directory where to find logs; absolute path

# Only for testing purposes
host=$(hostname)

function check_esxi_du
{
	# Done - needs testing
	# I don't like the way this function works, I'd like to be able to dynamically find 
	IFS=$';'
	for du_vol in $(tail -1 $loghome/$host/vm_disk_use)   # fix
	do
	  echo $du_vol | grep "," &> /dev/null
	  retval=$?
	  if [ $retval -gt 0 ]
	  	then
	    du_timestamp=$du_vol
	  else
	    du_vol_name=$(echo $du_vol | awk -F"," '{print $1}')
	    du_vol_capacity=$(echo $du_vol | awk -F"," '{print $2}')
	    du_vol_free=$(echo $du_vol | awk -F"," '{print $3}')
	    du_vol_percentage=$(echo "$du_vol_capacity / ($du_vol_capacity - $du_vol_free)" | bc)
	    if [ $du_vol_percentage -lt $warn ]
	    	then
	      du_vol_tmp_holder=$(echo "OKAY:$du_vol_name:$du_vol_percentage:$(echo scale=2 $du_vol_capacity - $du_vol_free | bc | sed 's/0.//')/$du_vol_capacity:")
	    elif [ $du_vol_percentage -lt $cirt ]
	    	then
	      du_vol_tmp_holder=$(echo "WARNING:$du_vol_name:$du_vol_percentage:$(echo scale=2 $du_vol_capacity - $du_vol_free | bc | sed 's/0.//')/$du_vol_capacity:")
	    else
	      du_vol_tmp_holder=$(echo "CRITICAL:$du_vol_name:$du_vol_percentage:$(echo scale=2 $du_vol_capacity - $du_vol_free | bc | sed 's/0.//')/$du_vol_capacity:")
	    fi
	  fi
	  du_vol_message=$du_vol_message-$du_vol_tmp_holder
	done
	case $du_vol_message in
		*CRITICAL*)
			echo "CRITICAL: $du_vol_message"
			exit 2
			;;
		*WARNING*)
			echo "WARNING: $du_vol_message"
			exit 1
			;;
		*OKAY*)
			echo "OKAY: $du_vol_message"
			exit 0
			;;
		*)
			echo "UNKNOWN: $du_vol_message"
			exit 3
			;;
	esac
}

function check_esxi_vms
{
	# Done - needs testing
	# This function pulls all running vms on a host and will warn/crit based upon the number of vms running
	# It's a reverse threshold, so being underneath the threshold will cause a warn/crit
	# The purpose is not really to warn, as much as it is to get a status of the vms on a particular esx host

	IFS=$';'
	for vms in $(tail -1 $loghome/$host/vm_status)
	do
		echo $vms | grep "," &> /dev/null
		retval=$?
		if [ $retval -gt 0 ]
			then
			vms_timestamp=$vms
		else
			vm_message=$vm_message$(echo $vms | awk -F"," '{print $1}'),$(echo $vms | awk -F"," '{print $2}')
		fi
	done
	vm_result=$(echo $vm_message | sed 's/$\;//g')
	vm_total=$(echo $vm_result | tr ';' '\n' | wc -l)
	vm_run=$(echo $vm_result | tr ';' '\n' | grep running | wc -l)
	vm_reg=$(echo $vm_result | tr ';' '\n' | grep registerd | wc -l)
	case 
  if [ $(echo "$vm_run <= $crit" | bc) -eq 1 ]
  	then
 		echo "CRITICAL: Machines on this host is less than $crit $vm_result | running=$vm_run total=$vm_total"
 		exit 2
	elif [ $(echo "$vm_run <= $warn" | bc) -eq 1 ]
		then
		echo "WARNING: Machines on this host is less than $warn $vm_result | running=$vm_run total=$vm_total"
		exit 1
	elif [ $(echo "$vm_run > $crit" | bc) -eq 1 ]
		then
		echo "OKAY: Machines on this host are more than $warn $vm_result | running=$vm_run total=$vm_total"
		exit 0
	else
		echo "UNKNOWN: Not really sure what happened here $vm_result | running=$vm_run total=$vm_total"
		exit 3
	fi
}

function check_esxi_mem
{
	#done - needs testing
	#This function deals with memory usage based on a percent and will fail at values defined in nagios
	mem_timestamp=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $1}')
	mem_total=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $6}')
	mem_free=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $7}')
	mem_used=$(echo $mem_total - $mem_free | bc)
	mem_percent=$(echo scale=2 $mem_used / mem_total | bc | sed 's/0.//')
	if [ $(echo "$mem_percent >= $crit" | bc) -eq 1 ]
		then
		echo "CRITICAL: $mem_percent Memory Usage | time=$mem_timestamp use=$mem_percent"
		exit 2
		;;
	elif [ $(echo "$mem_percent >= $warn" | bc) -eq 1 ]
		echo "WARNING: $mem_percent Memory Usage | time=$mem_timestamp use=$mem_percent"
		exit 1
		;;
	elif [ $(echo "$mem_percent < $warn" | bc) -eq 1 ]
		echo "OKAY: $mem_percent Memory Usage | time=$mem_timestamp use=$mem_percent"
		exit 0
		;;
	else
		echo "UNKNOWN RESULTS: $mem_percent Memory Usage | time=$mem_timestamp use=$mem_percent"
		exit 3
		;;
	fi
}

function check_esxi_cpu
{
	# Done: need testing
	# This funciton will pull the most recent cpu use out of the log file and compare it to 
	# threshold defined in nagios
	cpu_use=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $5}')
	cpu_timestamp=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $1}')
	if [ $(echo "$cpu_use >= $crit" | bc) -eq 1 ]
		then
		echo "CRITICAL: $cpu_use CPU Usage | time=$cpu_timestamp use=$cpu_use"
		exit 2
		;;
	elif [ $(echo "$cpu_use >= $warn" | bc) -eq 1 ]
		echo "WARNING: $cpu_use CPU Usage | time=$cpu_timestamp use=$cpu_use"
		exit 1
		;;
	elif [ $(echo "$cpu_use < $warn" | bc) -eq 1 ]
		echo "OKAY: $cpu_use CPU Usage | time=$cpu_timestamp use=$cpu_use"
		exit 0
		;;
	else
		echo "UNKOWN STATE: $cpu_use CPU Usage | time=$cpu_timestamp use=$cpu_use"
		exit 3
		;;
	fi
}

function check_esxi_load
{
	#done - needs testing
	# This function will test the average system load over 1 5 and 15 minute intevervals.
	# If the load at any interval exceeds the warn/crit threshold, it will crit/warn
	load1=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $2}')
	load5=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $3}')
	load15=$(tail -1 $loghome/$host/vm_perf_data | awk '{print $4}')
	if [ $(echo "$load1 >= $crit" | bc) -eq 1 ] || [ $(echo "$load5 >= $crit" | bc) ] || [ $(echo "$load15 >= $crit" | bc) ]
		then
		echo "CRITICAL: load1=$load1 load5=$load5 load15=$load15 | l1=$load1 l5=$load5 l15=$laod15"
		exit 2
	elif [ $(echo "$load1 >= $warn" | bc) -eq 1 ] || [ $(echo "$load5 >= $war" | bc) ] || [ $(echo "$load15 >= $warn" | bc) ]
		then
		echo "WARNING: load1=$load1 load5=$load5 load15=$load15 | l1=$load1 l5=$load5 l15=$laod15"
		exit 1
	elif [ $(echo "$load1 < $warn" | bc) -eq 1 ] || [ $(echo "$load5 < $war" | bc) ] || [ $(echo "$load15 < $warn" | bc) ]
		then
		echo "OKAY: load1=$load1 load5=$load5 load15=$load15 | l1=$load1 l5=$load5 l15=$laod15"
		exit 0
	else
		echo "UNKNOWN: load1=$load1 load5=$load5 load15=$load15 | l1=$load1 l5=$load5 l15=$laod15"
		exit 3
	fi
}

function usage
{
echo "Usage:"
echo "check_esxi -C [cpu|vms|du|load|mem] -H [hostname] -w [warning threshold] -c [critical threshold]"
echo "	cpu:		Checks cpu usage"
echo "	vms:		Gets listing of virtual machines"
echo "	du:			Gets disk usage of a esx host"
echo "	load:		Gets load average over 1, 5, 15 minutes. reports them all together"
echo "	mem:		Checks Memory usage on a esx host"
echo
}


function main
{
	while getopts ":Hwc:C" opts
	do
		case $opts in
			C)
				case $OPTARG in 
					cpu)
						check_esxi_cpu 
						;;
					vms)
						check_esxi_vms
						;;
					du)
						check_esxi_du
						;;
					load)
						check_esxi_load
						;;
					mem)
						check_esxi_mem
						;;
					*)
						usage
						exit 3
						;;
				esac
				;;
			H)
				host=$OPTARG
				;;
			w)
				warn=$OPTARG
				;;
			c)
				crit=$OPTARG
				;;
			*)
				usage
				exit 3
				;;
		esac
	done
}
