#!/bin/ash
#nagcol.sh
# Collects information from an esx host and sends it to a remote server for nagios processing

# Configuration
remote_host=corp-nagios1.prd.yaanatech.com                                              #nagios server
remote_base=/home/nagutil/nagcol                                                        #base directory to store the data
ssh_port=2022
identity_file=/opt/nagcol/.id_rsa                                                       #priv ssh key

# Configuration (not to be changed, but can be as long as it makes sense)
local_host=$(hostname)
timestamp=$(date -u +%F_%H%M%S)

cpu_mem()
{
        # Get cpu, memory and load statistics and store it in a variable
        # Output format:
        # timestamp;load 1;load 5;load 15;average cpu usage (all);total mem;free mem
        vm_perf_data=$timestamp\;$(/sbin/esxtop -b -n1 -import-entity /opt/nagcol/.esxtop50rc |  /bin/awk -F"," '{print $5,$6,$7,$82,$83,$90}' | /bin/tail -1 | sed 's/^"//g' | sed 's/"$//' | sed 's/" "/;/g' | sed 's/ /,/g')
}

disk_useage()
{
        # This function will gather disk usage and store the information in a variable
        # Output format volume_name,size,free;volume_name,size,free; .......
        IFS=$'\n'
        for raw_du in $(/sbin/esxcli storage filesystem list | /bin/grep true | /bin/grep -E "NFS|VMFS" | /bin/awk '{print $2,$6,$7}')
        do
                tmp_du=$(echo $raw_du | sed 's/ /,/g')
                combined_du=$combined_du\;$tmp_du
        done
        vm_disk_use=$timestamp$combined_du
        unset IFS
}

get_vms()
{
        # This fucntion will gather current VMs and whether or not they are running and store the information in a variable
        # ouput format vm,running;vm,registered;.........
        vm_status=$timestamp\;
        IFS=$'\n'
        for vm in $(vm-support -V)
        do
                vm_status=$vm_status$(/bin/echo -n $vm | /bin/awk -F"/" '{print $5}')\,
                vm_status=$vm_status$(/bin/echo $vm | /bin/awk -F" " '{print $2}' | /bin/sed 's/(//' | /bin/sed 's/)//')\;
        done
        unset IFS
}

send_stats()
{
        # This function will off load all the variable collected in the other fnctions to the designated host
        /bin/ssh -i $identity_file -p $ssh_port $remote_host /bin/touch /var/run/nagcol.$hostname
        for ssh_stats in vm_perf_data vm_disk_use vm_status
        do
                /bin/ssh -i $identity_file -p $ssh_port $remote_host $(eval /bin/echo \$$ssh_stats) >> $remote_base/$hostname/$ssh_stats
        done
        /bin/ssh -i $identity_file -p $ssh_port $remote_host /bin/rm -fi /var/run/nagcol.$hostname
}

make_remote_files()
{
        # This function will ensure that the directory structure on the remote host is what it is supposed to be
        # If not, it will create it.
        /bin/ssh -i $identity_file -p $ssh_port $remote_base/$hostname &> /dev/null
        if [ $? -ne "0" ]
        then
                /bin/ssh -i $identity_file -p $ssh_port $remote_host /bin/mkdir $remote_base/$hostname
        fi
        for check_files in vm_perf_data vm_disk_use vm_status
        do
                /bin/ssh -i $identity_file -p $ssh_port /bin/ls $remote_base/$hostname/$check_files &> /dev/null
                if [ $? -ne "0" ]
                then
                        /bin/ssh -i $identity_file -p $ssh_port $remote_host /bin/touch $remote_base/$hostname/$check_files
                fi
        done
}

main()
{
        disk_useage
        get_vms
        cpu_mem
        make_remote_files
        send_stats
}

main
exit 0

