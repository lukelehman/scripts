#!/bin/bash
#check_cert_age.bash
# This script will check the age of an ssl cert on the local system, compare it to the thresholds set in the command line, and alert if a value exceeds the threshold.
# The percision is in days.
#
# EXAMPLES:
#   CHECK SINGLE CERT: check_cert_age.bash -p /etc/pki/tls/mycert.crt -w 60 -c 30
#   CHECK DIR OF CERTS: check_cert_age.bash -d /etc/pki/tls/ -w 60 -c 30
#
# OPTIONS:
#   -w Warning Threshold
#   -c Critical Threshold
#   -p Path to cert
#   -d Path to directory with certs
#
# Cleaning up command line usage:
#   This script is designed to pass information to nagios. The output using directory option is one long single line.
#   To clean this up to make it more intellegable on the CLI, consider piping it through sed (or something similar).
#   Example:
#
#   check_cert_age.bash -d /path/to/cert/directory -w 60 -c 30 | sed 's/,/\n/g'
#
# Add the following line to a file in /etc/nrpe.d (or in /etc/nagios/nrpe.cfg)
#   Single cert:
#     command[check_cert_age]=/usr/lib64/nagios/plugins/check_cert_age.bash -p $ARG1$ -w $ARG2$ -c $ARG3$
#   Directory:
#     command[check_cert_age_dir]=/usr/lib64/nagios/plugins/check_cert_age.bash -d $ARG1$ -w $ARG2$ -c $ARG3$
#


# nagios exit states
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Get CLI options

unset $cert_dir
unset $cert_loc

while getopts :w:c:d:p: options
do
  case "$options" in
    w) warn=${OPTARG};;
    c) crit=${OPTARG};;
    p) cert_loc=${OPTARG};;
    d) cert_dir=$(echo ${OPTARG} | sed 's/\/$//g');;
    *) echo "something is wrong with you arguments, please check again"
  esac
done

function exit_text()
{
  # this function will figure out what output to echo to stdout (nrpe)
  if [ "$certs_unk" ]
    then
    echo -n "UNKOWN="
    echo -n $certs_unk
  fi
  if [ "$certs_over" ]
    then
    echo -n "OVER_DUE="
    echo -n $certs_over
  fi
  if [ "$certs_crit" ]
    then
    echo -n "CRIT="
    echo -n $certs_crit
  fi
  if [ "$certs_warn" ]
    then
    echo -n "WARN="
    echo -n $certs_warn
  fi
  if [ "$certs_ok" ]
    then
    echo -n "OK="
    echo -n $certs_ok
  fi
  echo
}

# If the options define a cert explicitly do this, or if direcotry, do the next thing
if [ "$cert_loc" ]
  then
  cert_age=$(echo "$(echo "$(date -d "$(openssl x509 -noout -in $cert_loc -dates | tail -1 | sed 's/notAfter=//')" +%s)" - "$(date +%s)" | bc)" / 60 / 60 / 24 | bc)
  # Compare cert_age to thresholds given, exit with status
  if [ "$cert_age" -le "$warn" ]
    then
    if [ "$cert_age" -le "$crit" ] && [ "$cert_age" -gt "0" ]
      then
      echo "CRITICAL, SSL cert is going to expire in  $cert_age days| age=$cert_age"
      exit $STATE_CRITICAL
    elif [ "$cert_age" -le 0 ]
      then
      echo "CRITICAL, SSL cert is going to expire in  $cert_age days| age=$cert_age"
      exit $STATE_CRITICAL
    elif [ "$cert_age" -ge "$crit" ] && [ "$cert_age" -le "$warn" ]
      then
      echo "WARNING, SSL cert is going to expire in  $cert_age days | age=$cert_age"
      exit $STATE_WARNING
    fi
  else
    if [ "$cert_age" -ge 0 ]
      then
      echo "OK, SSL cert is not expired | age=$cert_age"
      exit $STATE_OK
    else
    echo "This script has it's limitations and this is one of them, please fix it | age=$cert_age"
    exit $STATE_UNKNOWN
    fi
  fi
# do the next thing (directory)
elif [ "$cert_dir" ]
  then
  #initialize arrays
  unset $certs_warn
  unset $certs_crit
  unset $certs_over
  unset $certs_unk
  unset $certs_ok



  ####EDIT THE FOLLOWING LINE FOR INCLUSIONS AND EXCLUSIONS
  for cert_child in $(ls $cert_dir/* | egrep ".crt|.pem" | grep -v .rpmnew | grep -v localhost.crt)
  do
    # check the cert age and assign it to a variable
    cert_age=$(echo "$(echo "$(date -d "$(openssl x509 -noout -in $cert_child -dates | tail -1 | sed 's/notAfter=//')" +%s)" - "$(date +%s)" | bc)" / 60 / 60 / 24 | bc)
    cert_child=$(basename $cert_child)
    # Compare the cert age with the thresholds given, assign the file name to an array to the matching condition
    if [ "$cert_age" -le "$warn" ]
      then
      if [ "$cert_age" -le "$crit" ] && [ "$cert_age" -gt "0" ]
        then
        certs_crit+="$cert_child:$cert_age days until expiration, "
      elif [ "$cert_age" -le 0 ]
        then
        certs_over+="$cert_child:$cert_age days until expiration, "
      elif [ "$cert_age" -ge "$crit" ] && [ "$cert_age" -le "$warn" ]
        then
        certs_warn+="$cert_child:$cert_age days until expiration, "
      fi
    else
      if [ "$cert_age" -ge 0 ]
        then
        certs_ok+="$cert_child:$cert_age days until expiration, "
      else
        certs_unk+="$cert_child:$cert_age days until expiration, "
      fi
    fi
  done
  if [ "$certs_unk" ]
    then
    exit_text
    exit $STATE_UNKNOWN
  elif [ "$certs_crit" ]
    then
    exit_text
    exit $STATE_CRITICAL
  elif [ "$certs_warn" ]
    then
    exit_text
    exit $STATE_WARNING
  elif [ "$certs_ok" ]
    then
    exit_text
    exit $STATE_OK
  fi
fi
