#!/bin/bash
#scm-archiva_sync.bash
# Transforming url based lists (assuming use of ishi's archiva as source for artifact):
# note: This script expects a file with the folowing format: group.id:version:artifactid
#
# pattern used to scrape the artifact file for a certain format
# cat reallist | rev | awk -F/ '{$1=""; print}' | sed 's/^ //g' | sed 's/ /:/1' | sed 's/ /:/1' | rev | sed 's/ /\./g' | sort


##### Changelog ######
#4/22/15 - initial working version
#5/8/15 - Removed return from recursive function; added some comments
#6/8/15 - added bundle handling
######################



##### Configuration #####
# Absolute directory for the artifact list
artifact_list=
# Absolute diretory for Temorary store of artifacts  ### need function to clear this
temp_dir=
# remote archiva
rmt_archiva_url=
rmt_archiva_repo=internal
# local archiva
local_archiva_url=
local_archiva_repo=test
# ID that is specified in mavens global settings.xml, this file must have the appropriate user credentials tied to this ID
repo_id=
#########################


#unset the http_proxy environment variable
unset http_proxy
#unconfigurable
local_archiva_base_url=$local_archiva_url/archiva/repository/$local_archiva_repo
rmt_archiva_base_url=$rmt_archiva_url/archiva/repository/$rmt_archiva_repo

function split_aritfact_name()
{
    #this function will transform the line being input from the artifact list to usable data
    group_id=$(echo $1 | awk -F: '{print $1}')
    group_id_url=$(echo $1 | awk -F: '{print $1}' | sed 's/\./\//g')
    artifact_id=$(echo $1 | awk -F: '{print $2}')
    artifact_version=$(echo $1 | awk -F: '{print $3}')
}

function retrieve_artifact()
{
	file_extension=$1
	wget\
    	--output-document=$temp_dir/$artifact_id-$artifact_version.$file_extension \
    	$rmt_archiva_base_url/$group_id_url/$artifact_id/$artifact_version/$artifact_id-$artifact_version.$file_extension
}

function verify_artifact()
{
    # This function will compare the sha1sum from the remote archiva with the actual sha1sum from the file retrieved
    expected_sha_sum=$(cat $temp_dir/$artifact_id-$artifact_version.$1 | awk '{print $1}')
    actual_sha_sum=$(sha1sum $temp_dir/$artifact_id-$artifact_version.$2 | awk '{print $1}')
	if [ "$expected_sha_sum" == "$actual_sha_sum" ]
    then
        echo "$group_id:$artifact_id:$artifact_version did not fail sha1sum"
    else
    	if [ $count -ge 5 ]
        then
            echo "could not verify integrity of artifact after 5 attempts"
        else
            let "count++"
            verify_artifact $1 $2
        fi
    fi
}

function retrieve_upload_artifact()
{
    #This function will check to see if a .jar file exists for an artifact and retrieve it with the pom and subsequently deploy it (with the pom) to the local archiva
    retrieve_artifact pom
    retrieve_artifact pom.sha1
    verify_artifact pom.sha1 pom
    if wget --spider $rmt_archiva_base_url/$group_id_url/$artifact_id/$artifact_version/$artifact_id-$artifact_version.jar
    then
        retrieve_artifact jar
        retrieve_artifact jar.sha1
        verify_artifact jar.sha1 jar
        #deploy jar and pom
        echo "deploy jar and pom"
        if grep \<packaging\>bundle\</packaging\> $temp_dir/$artifact_id-$artifact_version.pom
            then
            #deploy the bundle
            mvn deploy:deploy-file \
                -DpomFile=$temp_dir/$artifact_id-$artifact_version.pom \
                -Dfile=$temp_dir/$artifact_id-$artifact_version.jar \
                -Durl=$local_archiva_base_url \
                -DrepositoryId=$repo_id \
                -DgeneratePom=false \
                -Dpackaging=bundle
            #deploy the jar
            mvn deploy:deploy-file \
                -DpomFile=$temp_dir/$artifact_id-$artifact_version.pom \
                -Dfile=$temp_dir/$artifact_id-$artifact_version.jar \
                -Durl=$local_archiva_base_url \
                -DrepositoryId=$repo_id \
                -DgeneratePom=false \
                -Dpackaging=jar
            else
            mvn deploy:deploy-file \
                -DpomFile=$temp_dir/$artifact_id-$artifact_version.pom \
                -Dfile=$temp_dir/$artifact_id-$artifact_version.jar \
                -Durl=$local_archiva_base_url \
                -DrepositoryId=$repo_id \
                -DgeneratePom=false
            fi
    else
        #deploy just pom
        echo "deploy just pom"
        mvn deploy:deploy-file \
            -DpomFile=$temp_dir/$artifact_id-$artifact_version.pom \
            -Dfile=$temp_dir/$artifact_id-$artifact_version.pom \
            -Durl=$local_archiva_base_url \
            -DrepositoryId=$repo_id \
            -DgeneratePom=false
    fi
}

function main()
{
    # This function will call other functions if it finds that the local archiva does not contain required artifact.
    for artifact_string in $(cat $artifact_list)
    do
        count=0
        split_aritfact_name $artifact_string
        if ! wget --spider $local_archiva_base_url/$group_id_url/$artifact_id/$artifact_version/$artifact_id-$artifact_version.pom ##check jar too?
            then
                retrieve_upload_artifact
            else
                echo "artifact already exists localy"
            fi
    done
}

main
exit 0
