#/bin/bash
#log-gnd.bash
# This script will parse files based upon the contents of the $log_disp_file. format for file:
# hostname,/path/to/log
# It will then generate configuration files and push them out to the rsyslog clients

#config
logrotate_template=~/log-gnd/logrotate.temp
rsyslog_client_conf=~/log-gnd/rsyslog.conf
global_imfile=~/log-gnd/0-global.conf
imfile_template=~/log-gnd/imfile.temp
log_disp_file=~/log-gnd/prd-log_disposition.csv
# paths to store logs temporarily
app_path=/root/log-gnd/temp/app
os_path=/log/os



# Do not configure
# External files
hn_list=$(awk -F',' '{print $1}' < $log_disp_file | uniq)


function check_dir_creation()
{
    if [ ! -d /root/log-gnd/temp/$1/$machine ]
        then
        mkdir -p /root/log-gnd/temp/$1/$machine
    fi
}

function log_os()
{
    for machine in $hn_list
    do
        check_dir_creation os
        for source_log_file in messages secure maillog cron spooler
        do
            cat "$imfile_template" | sed "s/_logloc_/\/var\/log\/$source_log_file/g" | sed "s/_logtag_/$source_log_file/g" | sed "s/_logfac_/local1/g" | sed "s/_readmode_/0/" >> $os_path/$machine/20-imfile-$source_log_file.conf
#            cat "$logrotate_template" | sed "s/_logloc_/\/var\/log\/$source_log_file/g" >> /log/os/$machine/logrotate.$source_log_file
        done

        for source_log_file in boot cmdlog
        do
            cat "$imfile_template" | sed "s/_logloc_/\/var\/log\/$source_log_file.log/g" | sed "s/_logtag_/$source_log_file/g" | sed "s/_logfac_/local1/g" | sed "s/_readmode_/0/" >> $os_path/$machine/20-imfile-$source_log_file.conf
            cat "$logrotate_template" | sed "s/_logloc_/\/var\/log\/$source_log_file.log/g" | sed "s/_logtag_/$log_tag.logrotate/g" >> $os_path/$machine/$source_log_file.logrotate
            cp $global_imfile $os_path/$machine
            cp $rsyslog_client_conf $os_path/$machine
        done
    done
}

function log_app()
{
    for log_disp_line in $(cat $log_disp_file)
    do
        machine=$(echo $log_disp_line | awk -F',' '{print $1}')
        log_loc=$(echo $log_disp_line | awk -F',' '{print $2}' | sed 's/\//\\\//g')
        log_tag=$(basename $log_loc .log)
        check_dir_creation app
        cat "$imfile_template" | sed "s/_logloc_/$log_loc/g" | sed "s/_logtag_/$log_tag/g" | sed 's/_logfac_/local0/g' | sed 's/_readmode_/2/' >> $app_path/$machine/20-imfile-$log_tag.conf
        cat "$logrotate_template" | sed "s/_logloc_/$log_loc/g" | sed "s/_logtag_/$log_tag.logrotate/g" >> $app_path/$machine/$log_tag.logrotate
    done
}

function push()
{
    # pushes files in given directory
    # usage:
    # push <Directory to base file structure> <wild card for filenames to send in each machines directory> <remote directory location>
    for machine_push2 in $1/*
    do
        remote_host=$(basename $machine_push2)
        for file_push in $machine_push2/$2
        do
            scpi $file_push $remote_host:$3/$(basename $file_push)
        done
    done
}

function main()
{
#   log_os
    log_app
    push $app_path 20-imfile-* /etc/rsyslog.d
#   push $os_path 20-imfile-* /etc/rsyslog.d
#   push $os_path 0-global.conf /etc/rsyslog.d
#   push $os_path rsyslog.conf /etc
    push $app_path *.logrotate /etc/logrotate.d
#   push $os_path *.logrotate /etc/logrotate.d
}
main
exit 0
