#/bin/bash
#check_log_dispostion.bash

LOG_DISP=prd-log_dispostion.csv

for FILE_ROW in $(cat $LOG_DISP)
do
echo $(echo $FILE_ROW | awk -F',' '{print $1}')
sshi $(echo $FILE_ROW | awk -F',' '{print $1}') "ls $(echo $FILE_ROW | awk -F',' '{print $2}')"
done
